# Espiralapp library to connect to the ecommerce api

## Run project

## Transactions

Import the main file

``` php
require_once dirname(__FILE__).'/lib/Espiralapp.php';
```

Add the api key that you want to use

``` php
use \Espiralapp\User;
User::setApiKey(YOUR APIKEY);
```

Create an instance of the Cart class

``` php
$cart = new \EspiralApp\Cart();
```

Add information to transaction

``` php
$orderData = $order->get_data();

$cardHolder = array(
  'cardHolder' => array (
    'name' => '',
    'email' => '',
    'phone' => '+52...'
  )
);
$address = array(
  'address' => array (
    'country' => 'MX',
    'state' => '',
    'city' => '',
    'numberExt' => '',
    'numberInt' => '',
    'zipCode' => '',
    'street' => '',
  )
);

$items_data['items'][] = array(
  'name' => 'producto1',
  'price' => 50,
  'description' => '',
  'quantity' => '1',
);
$items_data['items'][] = array(
  'name' => 'producto2',
  'price' => 50,
  'description' => '',
  'quantity' => '1',
);

$transaction = array(
  'transaction' => array (
    'total' => '100' // Transaction amount
  )
);
$metadata = array(
  'metadata' => array(
    'idOrder' => '0'
  )
);
$currency = array (
  'currency' => 'MXN'
);
$redirectUrl = array (
  'redirectUrl' => 'redirectSuccess',
  'backPage' => 'Client web page',
  'redirectData' => array (
    'url' => 'URL redirect data WebHook',
    'redirectMethod' => 'POST',
  )
);
```

Make Token

``` php
$response = $cart->sign($cardHolder, $address, $transaction, $items_data, $currency, $redirectUrl, $metadata);
```

Redirect checkout

``` php
$url = 'https://cart.espiralapp.com/' . $response['token']
```
